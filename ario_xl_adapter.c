#include <stdio.h>
#include <stdbool.h>
#include <getopt.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <clearblade.h>
#include <stdint.h>
#include <fcntl.h>
#include <linux/ioctl.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <errno.h>
#include <string.h>
#include <termios.h>
#include <ncurses.h>
#include <jansson.h>

#include "winsys_port.h"
#include "io60_spi_api.h"
#include "io60_spi_lib.h"
#include "io60_mio.h"
#include "io60_mio_adc8.h"
#include "io60_mio_dio24.h"

#define MAJOR_VER 1
#define MINOR_VER 0

static const char DeviceName[] = IO60_SPI_BASE_DEVICE_NAME "0";

static const uint16_t PinArray[DIO24_NUM_IO_PINS] = {
                                                         DIO24_PIN_0,
                                                         DIO24_PIN_1,
                                                         DIO24_PIN_2,
                                                         DIO24_PIN_3,
                                                         DIO24_PIN_4,
                                                         DIO24_PIN_5,
                                                         DIO24_PIN_6,
                                                         DIO24_PIN_7,
                                                         DIO24_PIN_8,
                                                         DIO24_PIN_9,
                                                         DIO24_PIN_10,
                                                         DIO24_PIN_11,
                                                         DIO24_PIN_12,
                                                         DIO24_PIN_13,
                                                         DIO24_PIN_14,
                                                         DIO24_PIN_15,
                                                         DIO24_PIN_16,
                                                         DIO24_PIN_17,
                                                         DIO24_PIN_18,
                                                         DIO24_PIN_19,
                                                         DIO24_PIN_20,
                                                         DIO24_PIN_21,
                                                         DIO24_PIN_22,
                                                         DIO24_PIN_23,
                                                      };

static const enum LTC1859_MUX_ADDRESS_ENUM ChannelNumberToMux[IO60_MIO_ADC_NUM_CHANNELS] = {
                                                                                                SINGLE_CHAN_0_ENUM,
                                                                                                SINGLE_CHAN_1_ENUM,
                                                                                                SINGLE_CHAN_2_ENUM,
                                                                                                SINGLE_CHAN_3_ENUM,
                                                                                                SINGLE_CHAN_4_ENUM,
                                                                                                SINGLE_CHAN_5_ENUM,
                                                                                                SINGLE_CHAN_6_ENUM,
                                                                                                SINGLE_CHAN_7_ENUM};

char *usage = "Usage: ./ario_xl_adapter --systemKey <YOUR_SYSTEMKEY> --systemSecret <YOUR_SYSTEMSECRET>"
        " --platformURL <YOUR_PLATFORMURL> --messagingURL <YOUR_MESSAGINGURL> --deviceName <YOUR_DEVICE_NAME>"
        " --activeKey <YOUR_DEVICE_ACTIVE_KEY> --mqttAdapterTopic <MQTT_TOPIC> --receiveAdapterTopic <RECEIVE_TOPIC> --readInterval <DATA_READ_INTERVAL_SECONDS>\n";

char *SYSTEM_KEY = NULL;
char *SYSTEM_SECRET = NULL;
char *DEVICE_NAME = NULL;
char *ACTIVE_KEY = NULL;
char *PLATFORM_URL = NULL;
char *MESSAGING_URL = NULL;
char *MQTT_CLIENT_ID = "ario_xl_adapter";
char *MQTT_TOPIC = NULL;
int MQTT_QOS = 0;
int READ_INTERVAL = 30;

int hSpiRegs; // descriptor for device access...
char *RECEIVE_TOPIC = NULL;

bool connected = false;

void cbInitCallback(bool error, char *result)
{
  if (error) 
  {
    printf("ClearBlade init failed %s\n", result);
    exit(-1);
  } 
  else 
  {
    printf("ClearBlade Init Succeeded\nAuth token: %s\n", result);
  }
}

void onConnect(void *context, MQTTAsync_successData *response)
{
  printf("Successful connection to MQTT Broker\n");
  extern int finished;
  finished = 1;
  connected = true;
}

int messageArrived(void *context, char *topicName, int topicLen, MQTTAsync_message *message) 
{ 
   uint8_t DioPort;
   uint8_t DioBit;
   int RetVal = STATUS_SUCCESS;
   
   printf("Message arrived: ");
   char *messagePayload = malloc(message->payloadlen + 1);
   strncpy(messagePayload, message->payload, message->payloadlen);
   messagePayload[message->payloadlen] = '\0';
  
  if (strcmp(topicName, RECEIVE_TOPIC) == 0)
  {
      
      printf("Topic:%s, ", topicName);
      printf("Message:%s\n", messagePayload);
      
      //read do_0
      if(strncmp(&messagePayload[8],"1",1) == 0 )
      {
         DioPort = 1;
         DioBit = 7;
         
         RetVal = io60_dio24_SetPinState(hSpiRegs, DIO24_MAKE_PIN(DioPort, 1 << DioBit), ENUM_PIN_SET);
         if (RetVal == STATUS_SUCCESS)
             printf("SUCCESS: Writing do_0 = 1, Port:%d Pin:%d\n", DioPort, DioBit);
         else if (RetVal == -STATUS_DIO_PIN_NOT_OUTPUT)
             printf("Error: Cannot write do_0 = 1, Port:%d Pin:%d, bit defined as an input\n", DioPort, DioBit);
         else 
             printf("\nError setting bit, do_0, - error %d\n", RetVal);
                        
      }
      else if (strncmp(&messagePayload[8],"0",1) == 0)
      {
         DioPort = 1;
         DioBit = 7;
         
         RetVal = io60_dio24_SetPinState(hSpiRegs, DIO24_MAKE_PIN(DioPort, 1 << DioBit), ENUM_PIN_CLEAR);
         if (RetVal == STATUS_SUCCESS)
             printf("SUCCESS: Writing do_0 = 0, Port:%d Pin:%d\n", DioPort, DioBit);
         else if (RetVal == -STATUS_DIO_PIN_NOT_OUTPUT)
             printf("Error: Cannot write do_0 = 0, Port:%d Pin:%d, bit defined as an input\n", DioPort, DioBit);
         else 
             printf("\nError clearing bit do_0, - error %d\n", RetVal);
          
      }
      else 
      {
         printf("Error: Cannot Read do_0 value\n");
      }
      
      //read do_1
      if(strncmp(&messagePayload[17],"1",1) == 0 )
      {
         DioPort = 1;
         DioBit = 6;
         
         RetVal = io60_dio24_SetPinState(hSpiRegs, DIO24_MAKE_PIN(DioPort, 1 << DioBit), ENUM_PIN_SET);
         if (RetVal == STATUS_SUCCESS)
             printf("SUCCESS: Writing do_1 = 1, Port:%d Pin:%d\n", DioPort, DioBit);
         else if (RetVal == -STATUS_DIO_PIN_NOT_OUTPUT)
             printf("Error: Cannot write do_1 = 1, Port:%d Pin:%d, bit defined as an input\n", DioPort, DioBit);
         else 
             printf("\nError setting bit, do_1, - error %d\n", RetVal);
      }
      else if (strncmp(&messagePayload[17],"0",1) == 0)
      {
         DioPort = 1;
         DioBit = 6;
         
         RetVal = io60_dio24_SetPinState(hSpiRegs, DIO24_MAKE_PIN(DioPort, 1 << DioBit), ENUM_PIN_CLEAR);
         if (RetVal == STATUS_SUCCESS)
             printf("SUCCESS: Writing do_1 = 0, Port:%d Pin:%d\n", DioPort, DioBit);
         else if (RetVal == -STATUS_DIO_PIN_NOT_OUTPUT)
             printf("Error: Cannot write do_1 = 0, Port:%d Pin:%d, bit defined as an input\n", DioPort, DioBit);
         else 
             printf("\nError clearing bit, do_1, - error %d\n", RetVal);
      }
      else 
      {
         printf("Error: Cannot Read do_1 value\n");
      }
      
      //read do_2
      if(strncmp(&messagePayload[26],"1",1) == 0 )
      {
         DioPort = 1;
         DioBit = 5;
         
         RetVal = io60_dio24_SetPinState(hSpiRegs, DIO24_MAKE_PIN(DioPort, 1 << DioBit), ENUM_PIN_SET);
         if (RetVal == STATUS_SUCCESS)
             printf("SUCCESS: Writing do_2 = 1, Port:%d Pin:%d\n", DioPort, DioBit);
         else if (RetVal == -STATUS_DIO_PIN_NOT_OUTPUT)
             printf("Error: Cannot write do_2 = 1, Port:%d Pin:%d, bit defined as an input\n", DioPort, DioBit);
         else 
             printf("\nError setting bit, do_2, - error %d\n", RetVal);
      }
      else if (strncmp(&messagePayload[26],"0",1) == 0)
      {
         DioPort = 1;
         DioBit = 5;
         
         RetVal = io60_dio24_SetPinState(hSpiRegs, DIO24_MAKE_PIN(DioPort, 1 << DioBit), ENUM_PIN_CLEAR);
         if (RetVal == STATUS_SUCCESS)
             printf("SUCCESS: Writing do_2 = 0, Port:%d Pin:%d\n", DioPort, DioBit);
         else if (RetVal == -STATUS_DIO_PIN_NOT_OUTPUT)
             printf("Error: Cannot write do_2 = 0, Port:%d Pin:%d, bit defined as an input\n", DioPort, DioBit);
         else 
             printf("\nError clearing bit, do_2, - error %d\n", RetVal);
      }
      else 
      {
         printf("Error: Cannot Read do_2 value\n");
      }
      
      //read do_3
      if(strncmp(&messagePayload[35],"1",1) == 0 )
      {
         DioPort = 1;
         DioBit = 4;
         
         RetVal = io60_dio24_SetPinState(hSpiRegs, DIO24_MAKE_PIN(DioPort, 1 << DioBit), ENUM_PIN_SET);
         if (RetVal == STATUS_SUCCESS)
             printf("SUCCESS: Writing do_3 = 1, Port:%d Pin:%d\n", DioPort, DioBit);
         else if (RetVal == -STATUS_DIO_PIN_NOT_OUTPUT)
             printf("Error: Cannot write do_3 = 1, Port:%d Pin:%d, bit defined as an input\n", DioPort, DioBit);
         else 
             printf("\nError setting bit, do_3, - error %d\n", RetVal);
      }
      else if (strncmp(&messagePayload[35],"0",1) == 0)
      {
         DioPort = 1;
         DioBit = 4;
         
         RetVal = io60_dio24_SetPinState(hSpiRegs, DIO24_MAKE_PIN(DioPort, 1 << DioBit), ENUM_PIN_CLEAR);
         if (RetVal == STATUS_SUCCESS)
             printf("SUCCESS: Writing do_3 = 0, Port:%d Pin:%d\n", DioPort, DioBit);
         else if (RetVal == -STATUS_DIO_PIN_NOT_OUTPUT)
             printf("Error: Cannot write do_3 = 0, Port:%d Pin:%d, bit defined as an input\n", DioPort, DioBit);
         else 
             printf("\nError clearing bit, do_3, - error %d\n", RetVal);
      }
      else 
      {
         printf("Error: Cannot Read do_3 value\n");
      }
      
       
  }
  else if (strcmp(topicName, MQTT_TOPIC) == 0) 
  {
      printf("Topic:%s, ", topicName);
      printf("Echo Message:%s\n", messagePayload);
  }
  else 
  {
     printf("Error Topic Does Not Exist: %s\n", topicName);  
  }
  
   MQTTAsync_freeMessage(&message);
   MQTTAsync_free(topicName);
   free(messagePayload);
   
   return 1;
}

void onConnectionLost(void *context, char *cause)
{
  printf("\nConnection lost\n");
  printf("Cause: %s\n", cause);
  printf("Reconnecting..\n");
  initializeClearBladeAsDevice(SYSTEM_KEY, SYSTEM_SECRET, PLATFORM_URL, MESSAGING_URL, DEVICE_NAME, ACTIVE_KEY, &cbInitCallback);
  connectToMQTTAdvanced(MQTT_CLIENT_ID, MQTT_QOS, &onConnect, &messageArrived, &onConnectionLost, true);
}

bool checkAllConstants()
{
  if (SYSTEM_KEY == NULL || SYSTEM_SECRET == NULL || PLATFORM_URL == NULL || MESSAGING_URL == NULL || DEVICE_NAME == NULL || ACTIVE_KEY == NULL || MQTT_TOPIC == NULL || RECEIVE_TOPIC == NULL)
    {
    return false;
  }
  return true;
}

void publishMessageToPlatform()
{
   
   int DioPort = 0,
        RetVal = STATUS_SUCCESS;

   uint8_t AdcChan,
            DioBit,
            DioArioChannel;
   
   enum PIN_STATE_ENUM PinState;
   
   struct IO60_ADC_CHANNEL_STRUCT ADC_Channel[2];        
            
  char theValue[50];
  char theKey[50];

    json_t *msg_json = json_object();
    char* msg_json_string = NULL;
  
     //Read all ADC Channels
   for (AdcChan = 0; AdcChan <= 7; AdcChan++)
   {

      ADC_Channel[0].ChannelConfig.Bits.ChannelAddress = ChannelNumberToMux[AdcChan];
      ADC_Channel[0].ChannelConfig.Bits.Polarity = UNIPOLAR_ENUM;
      ADC_Channel[0].ChannelConfig.Bits.Gain = GAIN_HIGH_ENUM;
      ADC_Channel[0].ChannelConfig.Bits.Nap = NAP_MODE_POWER_ON_ENUM;
      ADC_Channel[0].ChannelConfig.Bits.Sleep = SLEEP_MODE_POWER_ON_ENUM;

      // dummy read to flush read buffer
      io60_adc8_GetVolts(hSpiRegs, &ADC_Channel[0], 1);

      RetVal = io60_adc8_GetVolts(hSpiRegs, &ADC_Channel[0], 1);

      if (RetVal == STATUS_SUCCESS)
      {
          sprintf(theKey, "ai%d", AdcChan);
          printf("key: %s\n", theKey);
          sprintf(theValue, "(0x%04x,%fV)", ADC_Channel[0].ChannelData, ADC_Channel[0].ChannelVoltage);
          printf("value: %s\n",theValue);
          json_object_set_new(msg_json, theKey, json_string(theValue));
      }
      else
      {
          printf("\nError reading ADC - error %d\n", RetVal);
      }
       
      //memset(message, 0, sizeof(message));
   }
   
   DioPort = 0;
    
   for (DioBit = 7, DioArioChannel = 0; DioArioChannel <= 7; DioBit--, DioArioChannel++)
   {
      
      RetVal = io60_dio24_GetPinState(hSpiRegs, DIO24_MAKE_PIN(DioPort, 1 << DioBit), &PinState);

      if (RetVal == STATUS_SUCCESS)
      {
          sprintf(theKey, "di%d", DioArioChannel);
          printf("the key: %s\n", theKey);
          sprintf(theValue,"0b%d", PinState);
          printf("the value: %s\n", theValue);
          json_object_set_new(msg_json, theKey, json_string(theValue));
      }
      else if (RetVal == -STATUS_DIO_PIN_NOT_INPUT)
      {
          printf("\nCannot read a bit defined as an output\n");
      }
      else
      {
          printf("\nError reading DIO - error %d\n", RetVal);
      }
     
      memset(theKey, 0, sizeof(theKey));
      memset(theValue, 0, sizeof(theValue));
   }

   msg_json_string = json_dumps(msg_json, 0);
   printf("raw json: %s\n", msg_json_string);
   publishMessage(msg_json_string, MQTT_TOPIC, 0, 0);
   json_decref(msg_json);
  
}

void winSystemsInitialize()  
{
    int RetVal = STATUS_SUCCESS;

    enum PIN_DIRECTION_ENUM PinDir[] = {ENUM_PIN_DIR_IN, ENUM_PIN_DIR_IN, ENUM_PIN_DIR_IN, ENUM_PIN_DIR_IN, 
                                             ENUM_PIN_DIR_IN, ENUM_PIN_DIR_IN, ENUM_PIN_DIR_IN, ENUM_PIN_DIR_IN, 
                                             ENUM_PIN_DIR_IN, ENUM_PIN_DIR_IN, ENUM_PIN_DIR_IN, ENUM_PIN_DIR_IN, 
                                             ENUM_PIN_DIR_OUT, ENUM_PIN_DIR_OUT, ENUM_PIN_DIR_OUT, ENUM_PIN_DIR_OUT, 
                                             ENUM_PIN_DIR_IN, ENUM_PIN_DIR_IN, ENUM_PIN_DIR_IN, ENUM_PIN_DIR_IN, 
                                             ENUM_PIN_DIR_IN, ENUM_PIN_DIR_IN, ENUM_PIN_DIR_IN, ENUM_PIN_DIR_IN};

    printf("M410 Application Version %d.%d\n", MAJOR_VER, MINOR_VER);

    // command line is good so open device ...
    hSpiRegs = open(DeviceName, O_RDWR);

    if (hSpiRegs == -1)
    {
        printf("%s failed to open, errno = %d, %s\n", DeviceName, errno, strerror(errno));
    }
    else
    {
        //
        // got a "handle" to the SPI register set in IO space...
        //

        uint32_t Version;

        RetVal = ioctl(hSpiRegs, IOCTL_GET_VERSION, (puint32_t)&Version);
        if (RetVal)
        {
            printf("%s IOCTL call %u failed to open, errno = %d, %s\n", DeviceName, IOCTL_GET_VERSION, errno, strerror(errno));
        }
        else
        {
            uint8_t RevID;

            printf("SPI Driver v%02x.%02x.%04x\n",
                        (uint8_t)((Version & 0xff000000) >> 24),
                        (uint8_t)((Version & 0x00ff0000) >> 16),
                        (uint16_t)(Version & 0x0000ffff));

            io60_mio_GetRevID(hSpiRegs, &RevID);
            printf("FPGA revision code: 0x%02x\n\n", RevID);

            // initialize dio24
            RetVal = io60_dio24_Initialize(hSpiRegs);

            if (RetVal == STATUS_SUCCESS)
            {
                for (int i = 0; i < DIO24_NUM_IO_PINS; i++)
                {
                    if (io60_dio24_SetPinDirection(hSpiRegs, (enum DIO24_PIN_ENUM)PinArray[i], (enum PIN_DIRECTION_ENUM)PinDir[i], ENUM_PIN_SET) != STATUS_SUCCESS)
                    {
                        RetVal = -STATUS_IOCTL_FAILURE;
                        break;
                    }
                    else
                        printf("DIO Port %d Bit %d -> %s\n", i / 8, i % 8, PinDir[i] ? "Output" : "Input");
                }
            }

            printf("\n");

            for (int i = 0; i < IO60_MIO_ADC_NUM_CHANNELS; i++)
                printf("ADC Channel %d -> %s\n", i, "0-10V, Single-Ended");
        }
    }
}

int main(int argc, char *argv[])
{
  srand(time(NULL));
  struct option long_opt[] = {
    {"systemKey", required_argument, NULL, 'k'},
    {"systemSecret", required_argument, NULL, 's'},
    {"platformURL", required_argument, NULL, 'p'},
    {"messagingURL", required_argument, NULL, 'm'},
    {"deviceName", required_argument, NULL, 'd'},
    {"activeKey", required_argument, NULL, 'a'},
    {"mqttAdapterTopic", required_argument, NULL, 't'},
    {"receiveAdapterTopic", required_argument, NULL, 'r'},
    {"readInterval", required_argument, NULL, 'i'},
    {NULL, 0, 0, 0},
  };
  if (argc <= 1)
  {
    printf("%s", usage);
    return -1;
  }
  if (argc < 19)
  {
    printf("Not enough arguments\n%s", usage);
    return -1;
  }
  if (argc > 19)
  {
    printf("Received more arguments than required\n%s", usage);
    return -1;
  }
  int c;
  while ((c = getopt_long(argc, argv, "k:s:p:m:d:a:t:r:i:", long_opt, NULL)) != -1)
  {
    switch (c)
    {
      case 'k':
      {
        SYSTEM_KEY = optarg;
        printf("System Key is: %s\n", SYSTEM_KEY);
        break;
      }
      case 's':
      {
        SYSTEM_SECRET = optarg;
        printf("System Secret is: %s\n", SYSTEM_SECRET);
        break;
      }
      case 'p':
      {
        PLATFORM_URL = optarg;
        printf("Platform URL is: %s\n", PLATFORM_URL);
        break;
      }
      case 'm':
      {
        MESSAGING_URL = optarg;
        printf("Messaging URL is: %s\n", MESSAGING_URL);
        break;
      }
      case 'd':
      {
        DEVICE_NAME = optarg;
        printf("Device Name is: %s\n", DEVICE_NAME);
        break;
      }
      case 'a':
      {
        ACTIVE_KEY = optarg;
        printf("Device Active Key is: %s\n", ACTIVE_KEY);
        break;
      }
      case 't':
      {
        MQTT_TOPIC = optarg;
        printf("MQTT Topic to publish on is: %s\n", MQTT_TOPIC);
        break;
      }
      case 'r': {
        RECEIVE_TOPIC = optarg;
        printf("Receive Topic to publish on is: %s\n", RECEIVE_TOPIC);
        break;
      }
      case 'i':
      {
        READ_INTERVAL = atoi(optarg);
        printf("IO read interval is: %i\n", READ_INTERVAL);
        break;
      }
    }
  }
  // initialize against the clearblade platform as a device and get a valid auto token
  initializeClearBladeAsDevice(SYSTEM_KEY, SYSTEM_SECRET, PLATFORM_URL, MESSAGING_URL, DEVICE_NAME, ACTIVE_KEY, &cbInitCallback);
  // connect to MQTT broker
  connectToMQTTAdvanced(MQTT_CLIENT_ID, MQTT_QOS, &onConnect, &messageArrived, &onConnectionLost, true);
  // subscribe to the MQTT topic
  subscribeToTopic(MQTT_TOPIC, MQTT_QOS);
  subscribeToTopic(RECEIVE_TOPIC, MQTT_QOS);
 //Initialize WinSystems Hardware
  winSystemsInitialize(); 
  
  while (1) 
  {
    publishMessageToPlatform();
    sleep(READ_INTERVAL);
  }
  return 0;
}