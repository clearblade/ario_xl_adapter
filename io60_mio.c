//****************************************************************************
//
//    Copyright 2015 by WinSystems Inc.
//
//    Permission is hereby granted to the purchaser of WinSystems GPIO cards
//    and CPU products incorporating a GPIO device, to distribute any binary
//    file or files compiled using this source code directly or in any work
//    derived by the user from this file. In no case may the source code,
//    original or derived from this file, be distributed to any third party
//    except by explicit permission of WinSystems. This file is distributed
//    on an "As-is" basis and no warranty as to performance or fitness of pur-
//    poses is expressed or implied. In no case shall WinSystems be liable for
//    any direct or indirect loss or damage, real or consequential resulting
//    from the usage of this source code. It is the user's sole responsibility
//    to determine fitness for any considered purpose.
//
///****************************************************************************
//
//    Name       : io60_mio.c
//
//    Project    : SBC35-C405
//
//    Author     : pjp
//
//
///****************************************************************************
//
//      Date      Revision    Description
//    --------    --------    ---------------------------------------------
//    01/28/15      0.1       Original Release
//
///****************************************************************************
#undef __KERNEL_BUILD

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <fcntl.h>
#include <linux/ioctl.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <errno.h>
#include <math.h>

#include "winsys_port.h"
#include "io60_spi_api.h"
#include "io60_spi_lib.h"
#include "io60_mio.h"

///////////////////////////////////////////////////////////////////////////////
//
// some static variables...
//

///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
//
// Function: io60_dio24_GetRevID
//
// Description:
//    Reads the FPGA's revision ID register and returns the value.
//
// Parameters:
//     int          hSpiRegs    File descriptor (handle) for the SPI register
//                              port IO device
//     puint8_t     pRevID      Pointer to unsigned short integer where the
//                              revision ID will be written to
//
// Notes: TESTED
//
///////////////////////////////////////////////////////////////////////////////
int io60_mio_GetRevID( int hSpiRegs, puint8_t pRevID )
{
   uint8_t  TxBuffer[ 3 ] = {
                               MIO_DIO_CS,              // chip select for DIO24
                               CMD_READ_REVID,          // command
                               0xff                     // dummy...
                            };
   uint8_t  RxBuffer;

   int      RetVal  = STATUS_SUCCESS;

   if ( ( ! pRevID ) || ( ! hSpiRegs ) )
       {
          RetVal = -EINVAL;
       }
   else
       {
          SPI_XFER_STRUCT Transaction = {
                                           .pTxBuffer = ( puint8_t ) &TxBuffer,
                                           .pRxBuffer = ( puint8_t ) &RxBuffer,
                                           .TxLen = 3,
                                           .RxLen = 1,
                                        };

          if ( io60_spi_WriteThenRead( hSpiRegs, &Transaction ) )
              {
                 // failure

                 RetVal = -STATUS_IOCTL_FAILURE;
              }
          else
              {
                 //
                 // success
                 //

                 *pRevID = RxBuffer;
              }
       }

   return RetVal;
}

