//****************************************************************************
//
//    Copyright 2015 by WinSystems Inc.
//
//    Permission is hereby granted to the purchaser of WinSystems GPIO cards
//    and CPU products incorporating a GPIO device, to distribute any binary
//    file or files compiled using this source code directly or in any work
//    derived by the user from this file. In no case may the source code,
//    original or derived from this file, be distributed to any third party
//    except by explicit permission of WinSystems. This file is distributed
//    on an "As-is" basis and no warranty as to performance or fitness of pur-
//    poses is expressed or implied. In no case shall WinSystems be liable for
//    any direct or indirect loss or damage, real or consequential resulting
//    from the usage of this source code. It is the user's sole responsibility
//    to determine fitness for any considered purpose.
//
///****************************************************************************
//
//    Name       : io60_dio24.h
//
//    Project    : SBC35-C405
//
//    Author     : pjp
//
//    Include file for IO60-MIO-DIO24
//
///****************************************************************************
//
//      Date      Revision    Description
//    --------    --------    ---------------------------------------------
//    01/13/15      0.1       Original Release
//
///****************************************************************************

#ifndef _IO60_MIO_DIO24_H
#define _IO60_MIO_DIO24_H
///////////////////////////////////////////////////////////////////////////////
//
// Define additional errors specific to the DIO24 part of the IO60-M410 board
//

#define STATUS_DIO_GENERIC_ERROR               ( STATUS_GENERIC_ERROR + 1 )
#define STATUS_DIO_NOT_INITIALIZED             ( STATUS_DIO_GENERIC_ERROR + 1 )
#define STATUS_DIO_PIN_NOT_OUTPUT              ( STATUS_DIO_NOT_INITIALIZED + 1 )
#define STATUS_DIO_PIN_NOT_INPUT               ( STATUS_DIO_PIN_NOT_OUTPUT + 1 )
#ifdef __DEBUG
#define SPI_DEVICE_CLOCK_RATE       1000000         // 1.0 MHz
#else
#define SPI_DEVICE_CLOCK_RATE      11000000         // 11 MHz
#endif
#define DIO24_NUM_PORTS             3

#define DIO24_NUM_IO_PINS       24

#define DIO24_NUM_PORTS          3


//
//
enum DIO24_PIN_BIT_POS_ENUM {
                               BIT_POS_0 = 0x01,
                               BIT_POS_1 = 0x02,
                               BIT_POS_2 = 0x04,
                               BIT_POS_3 = 0x08,
                               BIT_POS_4 = 0x10,
                               BIT_POS_5 = 0x20,
                               BIT_POS_6 = 0x40,
                               BIT_POS_7 = 0x80,
                            };

//

//


#define DIO24_CMD_WRITE_GPIO        0x01
#define DIO24_CMD_READ_GPIO         0x05
#define DIO24_CMD_READ_REVID        0x9F

//
// define the GPIO register addresses
//

enum IO60_DIO24_PORT_ADDRESS_ENUM {
                                     DIO24_ADDR_PORT_0 =                0x00,
                                     DIO24_ADDR_PORT_1 =                0x01,
                                     DIO24_ADDR_PORT_2 =                0x02,
                                     DIO24_ADDR_PORT_0_RE_INT_EN =      0x10,
                                     DIO24_ADDR_PORT_1_RE_INT_EN =      0x11,
                                     DIO24_ADDR_PORT_2_RE_INT_EN =      0x12,
                                     DIO24_ADDR_PORT_0_FE_INT_EN =      0x20,
                                     DIO24_ADDR_PORT_1_FE_INT_EN =      0x21,
                                     DIO24_ADDR_PORT_2_FE_INT_EN =      0x22,
                                     DIO24_ADDR_PORT_0_CLEAR_INT =      0x30,
                                     DIO24_ADDR_PORT_1_CLEAR_INT =      0x31,
                                     DIO24_ADDR_PORT_2_CLEAR_INT =      0x32,
                                     DIO24_ADDR_PORT_0_INT_STATUS =     0x40,
                                     DIO24_ADDR_PORT_1_INT_STATUS =     0x41,
                                     DIO24_ADDR_PORT_2_INT_STATUS =     0x42,
                                  };

//////////////////////////////////////////////////////////////////////////////
//
// an enumeration for the LED state ( 0 = on, 1 = off )...
//
enum LED_STATE_ENUM {
                       ENUM_LED_OFF = 1,
                       ENUM_LED_ON = 0,
                    };

//
// same for a pin state...
//
enum PIN_STATE_ENUM {
                       ENUM_PIN_SET = 1,
                       ENUM_PIN_CLEAR = 0,
                    };

enum PIN_DIRECTION_ENUM {
                       ENUM_PIN_DIR_IN = 0,
                       ENUM_PIN_DIR_OUT = 1,
                    };


//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////

//
// An enumeration of the GPIO pins on the DIO24 of the IO60-MIO board. The
// enumeration of each pin will contain the pin's bit position in the lower
// byte and the DIO24 "port" that contains the pin in the upper byte. The
// DIO24 board has 3 ports, each with 8 IO pins.
//
enum DIO24_PIN_ENUM {
                       DIO24_PIN_0  = 0x0001,            // port 0
                       DIO24_PIN_1  = 0x0002,
                       DIO24_PIN_2  = 0x0004,
                       DIO24_PIN_3  = 0x0008,
                       DIO24_PIN_4  = 0x0010,
                       DIO24_PIN_5  = 0x0020,
                       DIO24_PIN_6  = 0x0040,
                       DIO24_PIN_7  = 0x0080,

                       DIO24_PIN_8  = 0x0101,            // port 1
                       DIO24_PIN_9  = 0x0102,
                       DIO24_PIN_10 = 0x0104,
                       DIO24_PIN_11 = 0x0108,
                       DIO24_PIN_12 = 0x0110,
                       DIO24_PIN_13 = 0x0120,
                       DIO24_PIN_14 = 0x0140,
                       DIO24_PIN_15 = 0x0180,

                       DIO24_PIN_16 = 0x0201,            // port 2
                       DIO24_PIN_17 = 0x0202,
                       DIO24_PIN_18 = 0x0204,
                       DIO24_PIN_19 = 0x0208,
                       DIO24_PIN_20 = 0x0210,
                       DIO24_PIN_21 = 0x0220,
                       DIO24_PIN_22 = 0x0240,
                       DIO24_PIN_23 = 0x0280
                    };

#define DIO24_MAKE_PIN( Port, BitNum )           ( ( ( Port & 0xf ) << 8 ) | BitNum )
#define DIO24_EXTRACT_PORT_FROM_PIN( Pin )       ( ( Pin & 0x0300 ) >> 8 )
#define DIO24_EXTRACT_BIT_POS_FROM_PIN( Pin )    ( Pin & 0xff )

////////////////////////////////////////////////////////////////////////////////
//
// prototypes of DIO24 functions exported by this module...
//
int io60_dio24_Initialize( int hSpiRegs );
int io60_dio24_SetPinDirection( int hSpiRegs, enum DIO24_PIN_ENUM PinID, enum PIN_DIRECTION_ENUM PinDirection, enum PIN_STATE_ENUM PinInitialState );
int io60_dio24_SetLedState( int hSpiRegs, enum LED_STATE_ENUM LedState );
int io60_dio24_ReadRegister( int hSpiRegs, enum IO60_DIO24_PORT_ADDRESS_ENUM RegAddr, puint8_t pRegisterData );
int io60_dio24_WriteRegister( int hSpiRegs, enum IO60_DIO24_PORT_ADDRESS_ENUM RegAddr, uint8_t RegData );
int io60_dio24_SetPinState( int hSpiRegs, enum DIO24_PIN_ENUM PinID, enum PIN_STATE_ENUM PinState );
int io60_dio24_GetPinState( int hSpiRegs, enum DIO24_PIN_ENUM PinID, enum PIN_STATE_ENUM *pPinState );

#endif  // _IO60_DIO24_H
