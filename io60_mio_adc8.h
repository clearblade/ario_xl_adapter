//****************************************************************************
//
//    Copyright 2015 by WinSystems Inc.
//
//    Permission is hereby granted to the purchaser of WinSystems GPIO cards
//    and CPU products incorporating a GPIO device, to distribute any binary
//    file or files compiled using this source code directly or in any work
//    derived by the user from this file. In no case may the source code,
//    original or derived from this file, be distributed to any third party
//    except by explicit permission of WinSystems. This file is distributed
//    on an "As-is" basis and no warranty as to performance or fitness of pur-
//    poses is expressed or implied. In no case shall WinSystems be liable for
//    any direct or indirect loss or damage, real or consequential resulting
//    from the usage of this source code. It is the user's sole responsibility
//    to determine fitness for any considered purpose.
//
///****************************************************************************
//
//    Name       : io60_adc8.h
//
//    Project    : SBC35-C405
//
//    Author     : pjp
//
//    Include file for IO60-MIO-ADC8
//
///****************************************************************************
//
//      Date      Revision    Description
//    --------    --------    ---------------------------------------------
//    02/10/15      0.1       Original Release
//
//  This include file defines constants and data structures for use with
//  the Linear Tech. LTC1859 8 Channel ADC chip present on the IO60-MIO
//  board.
//
///****************************************************************************

#ifndef _IO60_MIO_ADC8_H
#define _IO60_MIO_ADC8_H

#ifdef __DEBUG
#define ADC8_SPI_DEVICE_CLOCK_RATE        1000000         // 1.0 MHz
#else
#define ADC8_SPI_DEVICE_CLOCK_RATE       11000000         // 11MHz = 10.85 MHz, works...
#endif


#define LTC1859_REF_VOLTAGE         10.0            //

#define TWO_RAISED_16_MINUS_1       65535.0
#define TWO_RAISED_15_MINUS_1       32767.0

///////////////////////////////////////////////////////////////////////////////
//
// unions, structures, enumerations, etc. for the registers in the LTC1859
//

enum LTC1859_MUX_ADDRESS_ENUM {
                                 DIFF_POS0_NEG1_ENUM = 0x0,
                                 DIFF_POS2_NEG3_ENUM = 0x1,
                                 DIFF_POS4_NEG5_ENUM = 0x2,
                                 DIFF_POS6_NEG7_ENUM = 0x3,
                                 DIFF_POS1_NEG0_ENUM = 0x4,
                                 DIFF_POS3_NEG2_ENUM = 0x5,
                                 DIFF_POS5_NEG4_ENUM = 0x6,
                                 DIFF_POS7_NEG6_ENUM = 0x7,
                                 SINGLE_CHAN_0_ENUM  = 0x8,
                                 SINGLE_CHAN_2_ENUM  = 0x9,
                                 SINGLE_CHAN_4_ENUM  = 0xA,
                                 SINGLE_CHAN_6_ENUM  = 0xB,
                                 SINGLE_CHAN_1_ENUM  = 0xC,
                                 SINGLE_CHAN_3_ENUM  = 0xD,
                                 SINGLE_CHAN_5_ENUM  = 0xE,
                                 SINGLE_CHAN_7_ENUM  = 0xF,
                              };

enum LTC1859_POLARITY_ENUM {
                              BIPOLAR_ENUM = 0,
                              UNIPOLAR_ENUM = 1
                           };

enum LTC1859_GAIN_ENUM {
                          GAIN_LOW_ENUM = 0,
                          GAIN_HIGH_ENUM = 1
                       };

enum LTC1859_NAP_MODE_ENUM {
                              NAP_MODE_POWER_ON_ENUM = 0,
                              NAP_ENUM = 1
                           };

enum LTC1859_SLEEP_MODE_ENUM {
                                SLEEP_MODE_POWER_ON_ENUM = 0,
                                SLEEP_ENUM = 1
                             };

struct LTC1859_COMMAND_WORD_STRUCT {
                                      enum LTC1859_SLEEP_MODE_ENUM          Sleep:1;
                                      enum LTC1859_NAP_MODE_ENUM            Nap:1;
                                      enum LTC1859_GAIN_ENUM                Gain:1;
                                      enum LTC1859_POLARITY_ENUM            Polarity:1;
                                      enum LTC1859_MUX_ADDRESS_ENUM         ChannelAddress:4;
                                   };

union LTC1859_COMMAND_WORD_UNION {
                                    struct LTC1859_COMMAND_WORD_STRUCT  Bits;
                                    uint8_t                             Byte;
                                 };

///////////////////////////////////////////////////////////////////////////////
//
// #defines for use with the "Byte" field in LTC1859_COMMAND_WORD_UNION
//
// Single-Ended Channel Address
//
#define LTC1859_CH0             0x80
#define LTC1859_CH1             0xC0
#define LTC1859_CH2             0x90
#define LTC1859_CH3             0xD0
#define LTC1859_CH4             0xA0
#define LTC1859_CH5             0xE0
#define LTC1859_CH6             0xB0
#define LTC1859_CH7             0xF0
//
// Differential Channel Address
//
#define LTC1859_P0_N1           0x00
#define LTC1859_P1_N0           0x40
#define LTC1859_P2_N3           0x10
#define LTC1859_P3_N2           0x50
#define LTC1859_P4_N5           0x20
#define LTC1859_P5_N4           0x60
#define LTC1859_P6_N7           0x30
#define LTC1859_P7_N6           0x70
//
// Unipolar Mode Command
//
#define LTC1859_UNIPOLAR_MODE   0x08
#define LTC1859_BIPOLAR_MODE    0x00
//
// Gain Mode Command
//
#define LTC1859_LOW_GAIN_MODE   0x00
#define LTC1859_HIGH_GAIN_MODE  0x04
//
// Sleep Mode Command
#define LTC1859_SLEEP_MODE      0x01
#define LTC1859_NORMAL_MODE     0x00
#define LTC1859_NAP_MODE        0x02

//
//  Example command
//  adc_command = LTC1859_CH0 | LTC1859_UNIPOLAR_MODE | LTC1859_LOW_GAIN_MODE | LTC1859_NORMAL_MODE;     // Single-ended, CH0, unipolar, low gain, normal mode.
//
///////////////////////////////////////////////////////////////////////////////

#define IO60_MIO_ADC_NUM_CHANNELS       8

struct IO60_ADC_CHANNEL_STRUCT {
                                  union LTC1859_COMMAND_WORD_UNION      ChannelConfig;
                                  uint16_t                              ChannelData;
                                  float                                 ChannelVoltage;
                               };

typedef struct IO60_ADC_CHANNEL_STRUCT  *P_IO60_ADC_CHANNEL_STRUCT;

///////////////////////////////////////////////////////////////////////////////
//
// prototypes for functions related to the ADC8...
//
int io60_adc8_ReadChannel( int hSpiRegs, P_IO60_ADC_CHANNEL_STRUCT pChannelConfig );
int io60_adc8_GetVolts( int hSpiRegs, P_IO60_ADC_CHANNEL_STRUCT pChannelConfig, uint8_t ConvertToVolts );

#endif      // _IO60_MIO_ADC8_H
