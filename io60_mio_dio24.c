//****************************************************************************
//
//    Copyright 2015 by WinSystems Inc.
//
//    Permission is hereby granted to the purchaser of WinSystems GPIO cards
//    and CPU products incorporating a GPIO device, to distribute any binary
//    file or files compiled using this source code directly or in any work
//    derived by the user from this file. In no case may the source code,
//    original or derived from this file, be distributed to any third party
//    except by explicit permission of WinSystems. This file is distributed
//    on an "As-is" basis and no warranty as to performance or fitness of pur-
//    poses is expressed or implied. In no case shall WinSystems be liable for
//    any direct or indirect loss or damage, real or consequential resulting
//    from the usage of this source code. It is the user's sole responsibility
//    to determine fitness for any considered purpose.
//
///****************************************************************************
//
//    Name       : io60_mio.c
//
//    Project    : SBC35-C405
//
//    Author     : pjp
//
//
///****************************************************************************
//
//      Date      Revision    Description
//    --------    --------    ---------------------------------------------
//    01/28/15      0.1       Original Release
//
///****************************************************************************
#undef __KERNEL_BUILD

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <fcntl.h>
#include <linux/ioctl.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <errno.h>
#include <math.h>
#include <stdbool.h>

#include "winsys_port.h"
#include "io60_spi_api.h"
#include "io60_spi_lib.h"
#include "io60_mio.h"
#include "io60_mio_dio24.h"

///////////////////////////////////////////////////////////////////////////////
//

static uint8_t   PortShadowRegisterArray[ DIO24_NUM_PORTS ] = {
                                                                 0,
                                                                 0,
                                                                 0,
                                                              };

static uint8_t   PinDirectionArray[ DIO24_NUM_PORTS ];

static bool      DioInitialized = false;

///////////////////////////////////////////////////////////////////////////////
//
// Function: io60_dio24_Initialize
//
// Description: Set the specified IO pin LED to the specified state
//
// Parameters:
//    int   hSpiRegs    Device descriptor (handle) for IO60-SPI register
//                      device
//
// Notes:
//
///////////////////////////////////////////////////////////////////////////////
int io60_dio24_Initialize( int hSpiRegs )
{
   int      RetVal = STATUS_SUCCESS;
   uint8_t  i,
            Data = 0xFF;                        // and writing all port pins to 1 will float the pins...

   for ( i = 0; i < DIO24_NUM_PORTS; i++  )
   {
      PinDirectionArray[ i ] = 0xFF;            // set all pins as inputs

      if ( io60_dio24_WriteRegister( hSpiRegs, ( enum IO60_DIO24_PORT_ADDRESS_ENUM ) i, Data ) != STATUS_SUCCESS )
      {
         RetVal = -STATUS_IOCTL_FAILURE;
      }
   }

   DioInitialized = true;

   return RetVal;
}

///////////////////////////////////////////////////////////////////////////////
//
// Function: io60_dio24_SetPinDirection
//
// Description: Set the direction of the specified IO pin
//
// Parameters:
//    int   hSpiRegs    Device descriptor (handle) for IO60-SPI register
//                      device
//
//
//
// Notes:
//
///////////////////////////////////////////////////////////////////////////////
int io60_dio24_SetPinDirection( int hSpiRegs, enum DIO24_PIN_ENUM PinID, enum PIN_DIRECTION_ENUM PinDirection, enum PIN_STATE_ENUM PinInitialState )
{
   int RetVal = STATUS_SUCCESS;

   uint8_t   Pin = DIO24_EXTRACT_BIT_POS_FROM_PIN( PinID ),
             Port = DIO24_EXTRACT_PORT_FROM_PIN( PinID );

   if ( DioInitialized == true )
       {
          if ( Port < DIO24_NUM_PORTS )
              {
                 switch ( ( enum DIO24_PIN_BIT_POS_ENUM ) Pin )
                 {
                    case BIT_POS_0:
                    case BIT_POS_1:
                    case BIT_POS_2:
                    case BIT_POS_3:
                    case BIT_POS_4:
                    case BIT_POS_5:
                    case BIT_POS_6:
                    case BIT_POS_7:
                    {
                       if ( PinDirection == ENUM_PIN_DIR_OUT )
                           {
                              PinDirectionArray[ Port ] &= ~Pin;

                              if ( io60_dio24_SetPinState( hSpiRegs, PinID, PinInitialState ) != STATUS_SUCCESS )
                                  {
                                     RetVal = -STATUS_IOCTL_FAILURE;
                                  }
                              else
                                  {

                                  }
                           }
                       else
                           {
                              PinDirectionArray[ Port ] |= Pin;
                           }

                       break;
                    }

                    default:
                    {
                       RetVal = -STATUS_BAD_PARAMETER;
                       break;
                    }
                 }
              }
          else
              {
                 RetVal = -STATUS_BAD_PARAMETER;
              }
       }
   else
       {
          RetVal = -STATUS_DIO_NOT_INITIALIZED;
       }

   return RetVal;
}


///////////////////////////////////////////////////////////////////////////////
//
// Function: io60_dio24_GetPinState
//
// Description: Set the specified IO pin LED to the specified state
//
// Parameters:
//    int   hSpiRegs    Device descriptor (handle) for IO60-SPI register
//                      device
//
//
//
// Notes:
//
///////////////////////////////////////////////////////////////////////////////
int io60_dio24_GetPinState( int hSpiRegs, enum DIO24_PIN_ENUM PinID, enum PIN_STATE_ENUM *pPinState )
{
   int      Status = STATUS_SUCCESS;

   uint8_t  Port = DIO24_EXTRACT_PORT_FROM_PIN( PinID ),
            Pin = DIO24_EXTRACT_BIT_POS_FROM_PIN( PinID ),
            PortData;

   if ( DioInitialized == true )
       {
          if ( io60_dio24_ReadRegister( hSpiRegs, ( enum IO60_DIO24_PORT_ADDRESS_ENUM ) Port, &PortData ) != STATUS_SUCCESS )
              {
                 // failure

                 Status = STATUS_IOCTL_FAILURE;
              }
          else
              {
                 //
                 // success
                 //
                 // Got the current contents of the specified port.
                 // Use the Port Direction array to mask off all
                 // the outputs...
                 //

                 PortData &= PinDirectionArray[ Port ];

                 switch ( ( enum DIO24_PIN_BIT_POS_ENUM ) Pin )
                 {
                    case BIT_POS_0:
                    case BIT_POS_1:
                    case BIT_POS_2:
                    case BIT_POS_3:
                    case BIT_POS_4:
                    case BIT_POS_5:
                    case BIT_POS_6:
                    case BIT_POS_7:
                    {
                       if ( PortData & Pin )
                           {
                              //
                              // HW indicates pin is a '1'
                              //

                              *pPinState = ENUM_PIN_SET;
                           }
                       else
                           {
                              //
                              // HW indicates pin is a '0'
                              //

                              *pPinState = ENUM_PIN_CLEAR;
                           }

                       break;
                    }

                    default:
                    {
                       Status = -STATUS_BAD_PARAMETER;
                       break;
                    }
                 }  // switch
              }
       }
   else
       {
          Status = -STATUS_DIO_NOT_INITIALIZED;
       }

   return Status;
}



///////////////////////////////////////////////////////////////////////////////
//
// Function: io60_dio24_SetPinState
//
// Description: Set the specified IO pin LED to the specified state
//
// Parameters:
//    int   hSpiRegs    Device descriptor (handle) for IO60-SPI register
//                      device
//
//
//
// Notes: TESTED
//
///////////////////////////////////////////////////////////////////////////////
int io60_dio24_SetPinState( int hSpiRegs, enum DIO24_PIN_ENUM PinID, enum PIN_STATE_ENUM PinState )
{
   int      RetVal = STATUS_SUCCESS;
   uint8_t  PortNum,
            Bit;
   uint8_t  TxBuffer[ 4 ] = {
                               MIO_DIO_CS,              // chip select for DIO24 section of MIO
                               DIO24_CMD_WRITE_GPIO,    // command
                               0xFF,                    // Port # ( filled in below )
                               0xFF                     // port data ( filled in below )
                            };

   SPI_XFER_STRUCT Transaction = {
                                     .pTxBuffer = ( puint8_t ) &TxBuffer,
                                     .pRxBuffer = ( puint8_t ) 0,
                                     .TxLen = 4,
                                     .RxLen = 0,
                                    };

   if ( DioInitialized == true )
       {
          PortNum = DIO24_EXTRACT_PORT_FROM_PIN( PinID );   //  ( ( PinID & 0x0300 ) >> 8 );
          Bit = DIO24_EXTRACT_BIT_POS_FROM_PIN( PinID );    //  ( PinID & 0xFF );

          if ( ! ( Bit & PinDirectionArray[ PortNum ] ) )       // is pin an output pin ?
              {
                 TxBuffer[ MIO_DIO_PORT_OFFSET ] = PortNum;

                 if ( PinState == ENUM_PIN_CLEAR )
                     {
                        PortShadowRegisterArray[ PortNum ] &= ~Bit;
                     }
                 else if ( PinState == ENUM_PIN_SET )
                     {
                        PortShadowRegisterArray[ PortNum ] |= Bit;
                     }

                 //
                 // Need to write a 1 to any pin that is configured as an input.
                 // This will float the pin on the IO60_M410 which will allow any
                 // external circuit to drive the signal
                 //

                 PortShadowRegisterArray[ PortNum ] |= PinDirectionArray[ PortNum ];

                 //
                 // copy the data to the TX buffer for TX to target...
                 //

                 TxBuffer[ MIO_DIO_DATA_OFFSET ] = PortShadowRegisterArray[ PortNum ];

                 if ( io60_spi_WriteThenRead( hSpiRegs, &Transaction ) != STATUS_SUCCESS )     // send & check results
                     {
                        //
                        // some kind of failure
                        //

                        RetVal = -STATUS_IOCTL_FAILURE;
                     }
                 else
                     {
                        //
                        // success
                        //

                     }
              }
          else
              {
                 //
                 // pin direction is in, so let's not set/clear it, but
                 // instead flag an error...
                 //

                 RetVal = -STATUS_DIO_PIN_NOT_OUTPUT;
              }
       }
   else
       {
          RetVal = -STATUS_DIO_NOT_INITIALIZED;
       }

   return RetVal;
}

///////////////////////////////////////////////////////////////////////////////
//
// Function: io60_dio24_SetLedState
//
// Description: Set the LED to the specified state
//
// Parameters:
//    int   hSpiRegs    Device descriptor (handle) for IO60-SPI register
//                      device
//
//    enum LED_STATE_ENUM LedState      State to set LED to
//
// Notes: TESTED
//
///////////////////////////////////////////////////////////////////////////////
int io60_dio24_SetLedState( int hSpiRegs, enum LED_STATE_ENUM LedState )
{
   int      RetVal = STATUS_SUCCESS;

   if ( DioInitialized == true )
       {
          if ( hSpiRegs )
              {
                 enum PIN_STATE_ENUM   PinState;
                 int                   Temp;

                 //
                 // use API function to turn on/off LED...
                 //

                 if ( LedState == ENUM_LED_ON )
                      PinState = ENUM_PIN_CLEAR;
                 else PinState = ENUM_PIN_SET;

                 if ( ( Temp = io60_dio24_SetPinState( hSpiRegs, 1, PinState ) ) != STATUS_SUCCESS )
                 {
                    //
                    // some kind of error...
                    //

                    RetVal = Temp;
                 }
              }
          else
              {
                 //
                 // bad device handle...
                 //

                 RetVal = -STATUS_BAD_DEV_HANDLE;
              }
       }
   else
       {
          RetVal = -STATUS_DIO_NOT_INITIALIZED;
       }

   return RetVal;
}

///////////////////////////////////////////////////////////////////////////////
//
// Function: io60_dio24_ReadRegister
//
// Description:
//   Read the specified DIO24 port
//
// Parameters:
//     int          hSpiRegs    File descriptor (handle) for the SPI register
//                              port IO device
//
//     enum IO60_DIO24_PORT_ADDRESS_ENUM   RegAddr   The address in the FPGA
//                                                   for the specified port.
//
//     puint8_t     pRegData    Pointer to unsigned short int where register
//                              contents are to be written to.
//
// Notes: TESTED
//
///////////////////////////////////////////////////////////////////////////////
int io60_dio24_ReadRegister( int hSpiRegs, enum IO60_DIO24_PORT_ADDRESS_ENUM RegAddr, puint8_t pRegData )
{
   uint8_t  TxBuffer[ 4 ] = {
                               MIO_DIO_CS,              // chip select for DIO24
                               DIO24_CMD_READ_GPIO,     // command
                               RegAddr,
                               0xff                     // dummy...
                            };
   uint8_t  RxBuffer[ 2 ];

   int      RetVal  = STATUS_SUCCESS;

   SPI_XFER_STRUCT Transaction = {
                                    .pTxBuffer = ( puint8_t ) &TxBuffer,
                                    .pRxBuffer = ( puint8_t ) &RxBuffer,
                                    .TxLen = 4,
                                    .RxLen = 2,
                                 };

   //
   // Note - we do a read of 2 bytes from the SPI bus to address a bug in the IO60-DIO24 FPGA
   // implementation. The bug occurrs during reads - the FPGA sends the previous data value as
   // the 1st byte that is clocked out during the read phase of "Write Then Read". By telling the
   // FPGA to give us two bytes instead of one, and throwing away the 1st byte, the bug is
   // overcome and things work the way they should
   //

   if ( io60_spi_WriteThenRead( hSpiRegs, &Transaction ) )
       {
          // failure

          RetVal = -STATUS_IOCTL_FAILURE;
       }
   else
       {
          //
          // success
          //

          *pRegData = RxBuffer[ 1 ];
       }

   return RetVal;
}

///////////////////////////////////////////////////////////////////////////////
//
// Function: io60_dio24_WriteRegister
//
// Description: Write the specified data to the specified register
//
// Parameters:
//     int          hSpiRegs    File descriptor (handle) for the SPI register
//                              port IO device
//
//     enum IO60_DIO24_PORT_ADDRESS_ENUM   RegAddr   The address in the FPGA
//
//     uint8_t      RegData     Unsigned short int to write to specified port
//                              address
// Notes: TESTED
//
///////////////////////////////////////////////////////////////////////////////
int io60_dio24_WriteRegister( int hSpiRegs, enum IO60_DIO24_PORT_ADDRESS_ENUM RegAddr, uint8_t RegData )
{
   uint8_t  TxBuffer[ 4 ] = {
                               MIO_DIO_CS,              // chip select for DIO24
                               DIO24_CMD_WRITE_GPIO,    // command
                               RegAddr,
                               RegData
                            };
   uint8_t  RxBuffer;

   int      RetVal  = STATUS_SUCCESS;

   SPI_XFER_STRUCT Transaction = {
                                    .pTxBuffer = ( puint8_t ) &TxBuffer,
                                    .pRxBuffer = ( puint8_t ) &RxBuffer,
                                    .TxLen = 4,
                                    .RxLen = 0,
                                 };

   if ( io60_spi_WriteThenRead( hSpiRegs, &Transaction ) )
       {
          // failure

          RetVal = -STATUS_IOCTL_FAILURE;
       }
   else
       {
          //
          // success
          //

       }

   return RetVal;
}

