CC = gcc
CXX = g++

CFLAGS = -g -Wall -Werror -Wno-unused-variable -std=c99
CXXFLAGS = -g -Wall -Werror -Wno-unused-variable -std=c++0x


LIBS = -lclearblade -lpaho-mqtt3as -ljansson

SRCS_C = $(wildcard ./*.c)
SRCS_CPP = $(wildcard ./*.cpp)

OBJS_C = $(SRCS_C:.c=.o)
OBJS_CPP = $(SRCS_CPP:.cpp=.o)

MAIN = ario_xl_adapter

all: $(MAIN)
	@echo Compile complete. Execute main by ./ario_xl_adapter

$(MAIN): $(OBJS_C) $(OBJS_CPP)
	$(CXX) $(CFLAGS) -o $@ $^ $(LIBS)

clean:
	$(RM) -rf *.o ario_xl_adapter


