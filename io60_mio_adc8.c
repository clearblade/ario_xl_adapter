//****************************************************************************
//
//    Copyright 2015 by WinSystems Inc.
//
//    Permission is hereby granted to the purchaser of WinSystems GPIO cards
//    and CPU products incorporating a GPIO device, to distribute any binary
//    file or files compiled using this source code directly or in any work
//    derived by the user from this file. In no case may the source code,
//    original or derived from this file, be distributed to any third party
//    except by explicit permission of WinSystems. This file is distributed
//    on an "As-is" basis and no warranty as to performance or fitness of pur-
//    poses is expressed or implied. In no case shall WinSystems be liable for
//    any direct or indirect loss or damage, real or consequential resulting
//    from the usage of this source code. It is the user's sole responsibility
//    to determine fitness for any considered purpose.
//
///****************************************************************************
//
//    Name       : io60_mio_adc8.c
//
//    Project    : SBC35-C405
//
//    Author     : pjp
//
//
///****************************************************************************
//
//      Date      Revision    Description
//    --------    --------    ---------------------------------------------
//    02/18/15      0.1       Original Release
//
///****************************************************************************
#undef __KERNEL_BUILD

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <fcntl.h>
#include <linux/ioctl.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <errno.h>
#include <math.h>

#include "winsys_port.h"
#include "io60_spi_api.h"
#include "io60_spi_lib.h"
#include "io60_mio.h"
#include "io60_mio_adc8.h"

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//
// ADC8 functions
//
///////////////////////////////////////////////////////////////////////////////
//
// Function: io60_adc8_ReadChannel
//
// Description:
//   Performs a read of the ADC channel specified in the channel
//   configuration.
//
// Parameters:
//   int hSpiRegs       File descriptor for SPI port IO device driver
//
//   P_IO60_ADC_CHANNEL_STRUCT pChannelConfig  Pointer to channel configuration
//                                             structure
//
// Notes:
//
///////////////////////////////////////////////////////////////////////////////
int io60_adc8_ReadChannel( int hSpiRegs, P_IO60_ADC_CHANNEL_STRUCT pChannelConfig )
{
   int      RetVal  = STATUS_SUCCESS;

   uint8_t    TxBuffer[ 3 ] = {
		                         MIO_ADC_CS,
								 pChannelConfig->ChannelConfig.Byte,
								 0xFF
		                      },
              RxBuffer[ 2 ];

   SPI_XFER_STRUCT Xfer = {
		                     .pTxBuffer = ( puint8_t ) &TxBuffer,
							 .pRxBuffer = ( puint8_t ) &RxBuffer,
							 .TxLen = 3,
							 .RxLen = 0
                          };

   if ( io60_spi_WriteThenRead( hSpiRegs, &Xfer ) )
       {
          RetVal = -STATUS_IOCTL_FAILURE;
       }
   else
       {
	      Xfer.TxLen = 1;
	      Xfer.RxLen = 2;
          if ( io60_spi_WriteThenRead( hSpiRegs, &Xfer ) )
              {
        	     RetVal = -STATUS_IOCTL_FAILURE;
              }
          else
              {
                 pChannelConfig->ChannelData = ( uint16_t )( ( RxBuffer[ 0 ] << 8 ) | RxBuffer[ 1 ] );
              }
       }

   return RetVal;
}

///////////////////////////////////////////////////////////////////////////////
//
// Function: io60_adc8_GetVolts
//
// Description: Converts a 16 bit ADC sample to a voltage value
//
// Parameters:
//   int hSpiRegs       File descritor for SPI port IO device driver
//
//   P_IO60_ADC_CHANNEL_STRUCT pChannelConfig    Pointer to channel configuration
//                      structure
//
// Notes:
//
///////////////////////////////////////////////////////////////////////////////
int io60_adc8_GetVolts( int hSpiRegs, P_IO60_ADC_CHANNEL_STRUCT pChannelConfig, uint8_t ConvertToVolts )
{
  int   RetValue = STATUS_SUCCESS;

  if ( io60_adc8_ReadChannel( hSpiRegs, pChannelConfig ) != STATUS_SUCCESS )
      {
         RetValue = -STATUS_BAD_PARAMETER;
      }
  else if ( ConvertToVolts )
       {
          float Voltage,
                Sign = 1,
                RefVoltage = LTC1859_REF_VOLTAGE;

          uint16_t  ADC_Code = pChannelConfig->ChannelData;

          if ( pChannelConfig->ChannelConfig.Bits.Gain == GAIN_LOW_ENUM )
          {
             RefVoltage = RefVoltage / 2.0;
          }

          if ( pChannelConfig->ChannelConfig.Bits.Polarity == UNIPOLAR_ENUM )
              {
                 Voltage = ( ( ( float ) ADC_Code ) / TWO_RAISED_16_MINUS_1 );     // Calculate the input as a fraction of the
              }                                                                    // reference voltage (dimensionless)
          else
              {
                 if ( pChannelConfig->ChannelData & 0x8000 )                       // if MSB is set, ADC code is < 0
                 {
                    ADC_Code = ( ADC_Code ^ 0xFFFF ) + 1;                          // Convert ADC code from two's complement to binary
                    Sign = -1;
                 }

                 Voltage = ( Sign * ( float ) ADC_Code ) / TWO_RAISED_15_MINUS_1;  // Calculate the input as a fraction of the
              }                                                                    // reference voltage (dimensionless)

          pChannelConfig->ChannelVoltage = Voltage * RefVoltage;                   // Multiply fraction by Vref to get the actual
                                                                                  // voltage at the input (in volts)
       }

   return RetValue;
}

