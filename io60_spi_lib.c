//****************************************************************************
//
//    Copyright 2015 by WinSystems Inc.
//
//    Permission is hereby granted to the purchaser of WinSystems GPIO cards
//    and CPU products incorporating a GPIO device, to distribute any binary
//    file or files compiled using this source code directly or in any work
//    derived by the user from this file. In no case may the source code,
//    original or derived from this file, be distributed to any third party
//    except by explicit permission of WinSystems. This file is distributed
//    on an "As-is" basis and no warranty as to performance or fitness of pur-
//    poses is expressed or implied. In no case shall WinSystems be liable for
//    any direct or indirect loss or damage, real or consequential resulting
//    from the usage of this source code. It is the user's sole responsibility
//    to determine fitness for any considered purpose.
//
///****************************************************************************
//
//    Name       : io60_spi_lib.c
//
//    Project    : SBC35-C405
//
//    Author     : pjp
//
///****************************************************************************
//
//      Date      Revision    Description
//    --------    --------    ---------------------------------------------
//    01/28/15      0.1       Original Release
//
///****************************************************************************
#undef __KERNEL_BUILD

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <fcntl.h>
#include <linux/ioctl.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <errno.h>

#include "winsys_port.h"
#include "io60_spi_api.h"

///////////////////////////////////////////////////////////////////////////////
//
// Function:
//
// Description:
//
// Parameters:
//      <void>
//
// Notes:
//
///////////////////////////////////////////////////////////////////////////////
int io60_spi_WriteThenRead( int hSpiRegs, P_SPI_XFER_STRUCT pSpiXfer )
{
   return ioctl( hSpiRegs, IOCTL_SPI_WRITE_THEN_READ, ( pvoid ) pSpiXfer );
}

