///****************************************************************************
//
//    Copyright 2015 by WinSystems Inc.
//
//    Permission is hereby granted to the purchaser of WinSystems GPIO cards
//    and CPU products incorporating a GPIO device, to distribute any binary
//    file or files compiled using this source code directly or in any work
//    derived by the user from this file. In no case may the source code,
//    original or derived from this file, be distributed to any third party
//    except by explicit permission of WinSystems. This file is distributed
//    on an "As-is" basis and no warranty as to performance or fitness of pur-
//    poses is expressed or implied. In no case shall WinSystems be liable for
//    any direct or indirect loss or damage, real or consequential resulting
//    from the usage of this source code. It is the user's sole responsibility
//    to determine fitness for any considered purpose.
//
///****************************************************************************
//
//    Name       : io60_spi_lib.h
//
//    Project    : SBC35-C405 x86 Linux
//
//    Author     : Patrick Philp
//
///****************************************************************************
//
//      Date      Revision    Description
//    --------    --------    ---------------------------------------------
//    11/13/14      0.1       PJP - Original, from the open cores SPI master
//
///****************************************************************************

#ifndef _IO60_SPI_LIB_H
#define _IO60_SPI_LIB_H

//
// the name by which the driver may be accessed, after appending the
// chip select number...

#define IO60_SPI_BASE_DEVICE_NAME              "/dev/io60_spi_cs"

///****************************************************************************
//
// prototypes of the functions exported by the IO60-SPI "library"...
//

int         io60_spi_WriteThenRead( int hSpiRegs, P_SPI_XFER_STRUCT pSpiXfer );

#endif      // _IO60_SPI_LIB_H
