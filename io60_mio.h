//****************************************************************************
//
//    Copyright 2015 by WinSystems Inc.
//
//    Permission is hereby granted to the purchaser of WinSystems GPIO cards
//    and CPU products incorporating a GPIO device, to distribute any binary
//    file or files compiled using this source code directly or in any work
//    derived by the user from this file. In no case may the source code,
//    original or derived from this file, be distributed to any third party
//    except by explicit permission of WinSystems. This file is distributed
//    on an "As-is" basis and no warranty as to performance or fitness of pur-
//    poses is expressed or implied. In no case shall WinSystems be liable for
//    any direct or indirect loss or damage, real or consequential resulting
//    from the usage of this source code. It is the user's sole responsibility
//    to determine fitness for any considered purpose.
//
///****************************************************************************
//
//    Name       : io60_mio.h
//
//    Project    : SBC35-C405
//
//    Author     : pjp
//
//    Include file for IO60-MIO
//
///****************************************************************************
//
//      Date      Revision    Description
//    --------    --------    ---------------------------------------------
//    01/13/15      0.1       Original Release
//
///****************************************************************************

#ifndef _IO60_MIO_H
#define _IO60_MIO_H

//
// define the upper nyble values for each of the the MIO's 3 sub-functions
//  ( used by the MIO FPGA as slave chip selects to address a specific
//  sub function on the MIO board )
//

#define MIO_DIO_CS      0x01
#define MIO_ADC_CS      0x02
#define MIO_DAC_CS      0x04

///////////////////////////////////////////////////////////////////////////////
//
// some defines for the offset within the transmit buffer for various values
//
#define MIO_CHIP_SELECT_OFFSET      0
#define MIO_DIO_CMD_OFFSET          1
#define MIO_DIO_PORT_OFFSET         2
#define MIO_DIO_DATA_OFFSET         3

///////////////

#define CMD_READ_REVID        0x9F

///////////////////////////////////////////////////////////////////////////////////////////

int io60_mio_GetRevID( int hSpiRegs, puint8_t pRevID );


#endif      // _IO60_MIO_H
